/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import 'react-native-gesture-handler';
import React from 'react';
import {Provider} from 'react-redux';
import i18n from './src/i18n';
import store from './src/configStore';
import {I18nextProvider} from 'react-i18next';
import {NavigationContainer} from '@react-navigation/native';
import Navigation from './src/navigation/Navigation';
import {StatusBar} from 'react-native';

const App: () => React$Node = () => {
  return (
    <Provider store={store}>
      <StatusBar backgroundColor="white" barStyle={'dark-content'} />
      <NavigationContainer>
        <I18nextProvider i18n={i18n}>
          <Navigation />
        </I18nextProvider>
      </NavigationContainer>
    </Provider>
  );
};

export default App;
