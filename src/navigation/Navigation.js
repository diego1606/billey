import React from 'react';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {
  FirstNewBudgetNavigation,
  HomeBudgetNavigation,
  IndexView,
} from '../modules/budget';

const Drawer = createDrawerNavigator();

export default () => (
  <Drawer.Navigator initialRouteName="Index">
    <Drawer.Screen
      name={'Index'}
      component={IndexView}
      options={{
        swipeEnabled: false,
      }}
    />
    <Drawer.Screen name="Home" component={HomeBudgetNavigation} />
    <Drawer.Screen
      name={'First_new_budget'}
      component={FirstNewBudgetNavigation}
      options={{
        swipeEnabled: false,
      }}
    />
  </Drawer.Navigator>
);
