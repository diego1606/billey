import {budgetReducer} from './modules/budget';
import {applyMiddleware, combineReducers, createStore} from 'redux';
import thunk from 'redux-thunk';

export default createStore(
  combineReducers({
    budgetReducer,
  }),
  applyMiddleware(thunk),
);
