import messaging from '@react-native-firebase/messaging';
import PushNotificationIOS from '@react-native-community/push-notification-ios';
import PushNotification from 'react-native-push-notification';

class FCMService {
  checkPermissionFCM = () => {
    messaging()
      .hasPermission()
      .then((enabled) => {
        if (enabled) {
          //user has permission
          this.getToken();
        } else {
          //user don't have permission
          this.requestUserPermission();
        }
      })
      .catch((error) => {
        console.log('Permission rejected', error);
      });
  };

  requestUserPermission = async (): void => {
    const authorizationStatus = await messaging().requestPermission({
      alert: true,
      announcement: true,
      badge: true,
    });
    if (
      authorizationStatus === messaging.AuthorizationStatus.AUTHORIZED ||
      messaging.AuthorizationStatus.PROVISIONAL
    ) {
      this.getToken();
    } else {
      console.log('User has notification permissions disabled');
    }
  };

  getToken = (): void => {
    messaging()
      .getToken()
      .then((fcmToken) => {
        if (fcmToken) {
          console.log('fcmToken: ', fcmToken);
        } else {
          console.log('User does not have a device token');
        }
      })
      .catch((error) => {
        console.log('getToken rejected ', error);
      });
  };

  createNotificationListeners = (onRegister, onNotification) => {
    PushNotification.configure({
      // (optional) Called when Token is generated (iOS and Android)
      onRegister: (token) => {
        console.log('onRegister: ', token);
      },

      // (required) Called when a remote is received or opened, or local notification is opened
      onNotification: (notification) => {
        onNotification(notification);
        // (required) Called when a remote is received or opened, or local notification is opened
        notification.finish(PushNotificationIOS.FetchResult.NoData);
      },
      permissions: {
        alert: true,
        badge: true,
        sound: true,
      },
      popInitialNotification: true,
      requestPermissions: true,
    });
  };

  scheduleNotification = (
    message: string,
    date: Date,
    isBudgetEnd: boolean = true,
  ) => {
    const data = {
      isBudgetEnd,
    };
    PushNotification.localNotificationSchedule({
      message,
      data,
      date,
    });
  };
}

export default new FCMService();
