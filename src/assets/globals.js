export const globals = {
  FORMAT_DATE: 'DD-MM-YYYY',
  FORMAT_DATETIME: 'DD-MM-YYYY HH:mm:ss',
  NAME_DB: 'billey_offline.db',
  TIME_END_BUDGET: '21:00:00',
};
