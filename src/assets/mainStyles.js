import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 20,
    backgroundColor: 'white',
  },
  h1: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: 42,
  },
  text: {
    color: '#707070',
    fontSize: 14,
  },
  input: {
    height: 35,
    borderColor: '#e3e4e4',
    borderWidth: 1,
    paddingLeft: 10,
    borderRadius: 5,
  },
  inputLabel: {
    color: 'black',
    marginBottom: 10,
    fontWeight: '400',
  },
  btn: {
    width: '100%',
  },
  txtError: {
    color: '#dc3545',
  },
  txtSuccess: {
    color: '#28a745',
  },
  inputError: {
    height: 35,
    borderColor: '#dc3545',
    borderWidth: 1,
    paddingLeft: 10,
    borderRadius: 5,
  },
  textAlignCenter: {
    textAlign: 'center',
  },
  f1: {
    flex: 1,
  },
  f2: {
    flex: 2,
  },
  w100: {
    width: '100%',
  },
  inputContainerStyle: {
    borderBottomWidth: 0,
  },
  btnMenuRight: {
    paddingRight: 20,
  },
  btnMenuLeft: {
    paddingLeft: 20,
  },
  white: {
    color: 'white',
  },
  ph_1: {
    paddingHorizontal: 10,
  },
  pt_3: {
    paddingTop: 30,
  },
  mb_2: {
    marginBottom: 20,
  },
  clr_black: {
    color: 'black',
  },
  mt_1: {
    marginTop: 10,
  },
});
