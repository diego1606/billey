const View = require('react-native').View;
const mock = {
  Value: jest.fn(),
  event: jest.fn(),
  add: jest.fn(),
  eq: jest.fn(),
  set: jest.fn(),
  cond: jest.fn(),
  interpolate: jest.fn(),
  View: View,
  Extrapolate: {CLAMP: jest.fn()},
  Transition: {
    Together: 'Together',
    Out: 'Out',
    In: 'In',
  },
};

export default mock;
