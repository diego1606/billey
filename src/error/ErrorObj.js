export class ErrorObj extends Error {
  constructor(obj = {}, ...params) {
    super(...params);

    // Maintenir dans la pile une trace adéquate de l'endroit où l'erreur a été déclenchée (disponible seulement en V8)
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, ErrorObj);
    }
    this.name = 'ErrorObj';
    // Informations de déboguage personnalisées
    this.message = obj;
    this.date = new Date();
  }
}
