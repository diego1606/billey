import {Text, View} from 'react-native';
import React, {useState} from 'react';
import {Input} from 'react-native-elements';
import {mainStyles} from '../../../../assets';
import {useTranslation} from 'react-i18next';
import RNPickerSelect from 'react-native-picker-select';
import styles from './styles';
import Button from '../../../../components/Button/Button';

export const ExpenseForm = ({
  currency,
  dataPickerSelect,
  onSubmit,
  onCancel,
}) => {
  const {t} = useTranslation();
  const [amount, setAmount] = useState('');
  const [category, setCategory] = useState(null);
  const [errorAmount, setErrorAmount] = useState('');
  const [errorCategory, setErrorCategory] = useState('');

  let amountStr = 0;
  if (amount !== '') {
    amountStr = amount;
  }
  const handleSubmit = () => {
    let error = 0;
    if (amount === '') {
      error++;
      setErrorAmount(t('required'));
    }
    if (category === null) {
      error++;
      setErrorCategory(t('required'));
    }
    if (error <= 0) {
      setErrorAmount('');
      setErrorCategory('');
      onSubmit(amount, category);
    }
  };

  return (
    <>
      <View style={styles.containerInputExpenseForm}>
        <Text style={[mainStyles.inputLabel, styles.txtCategoryLbl]}>
          {t('category')}
        </Text>
        <RNPickerSelect
          placeholder={{
            label: t('select_category'),
            value: null,
          }}
          placeholderTextColor="#B0AFAF"
          onValueChange={(value) => setCategory(value)}
          items={dataPickerSelect}
          style={
            errorCategory === ''
              ? {
                  inputIOS: {
                    ...mainStyles.clr_black,
                  },
                  inputIOSContainer: {
                    ...styles.inputSelectContainer,
                  },
                  inputAndroidContainer: {
                    ...styles.inputSelectContainer,
                  },
                  inputAndroid: {
                    ...styles.inputAndroid,
                  },
                }
              : {
                  viewContainer: {
                    ...styles.inputError,
                  },
                }
          }
          useNativeAndroidPickerStyle={false}
        />
        {errorCategory === '' ? null : (
          <Text style={[styles.txtErrorPicker, mainStyles.txtError]}>
            {errorCategory}
          </Text>
        )}
        <Input
          errorMessage={errorAmount}
          label={t('amount')}
          placeholder={'0'}
          inputStyle={
            errorAmount === '' ? mainStyles.input : mainStyles.inputError
          }
          placeholderTextColor="#B0AFAF"
          labelStyle={[mainStyles.inputLabel, mainStyles.mt_1]}
          inputContainerStyle={mainStyles.inputContainerStyle}
          keyboardType="numeric"
          value={amount}
          errorStyle={mainStyles.txtError}
          onChangeText={(text) => {
            setAmount(text);
          }}
        />
      </View>
      <View style={styles.containerAmount}>
        <Text style={styles.txtAmount}>{`${currency} ${amountStr}`}</Text>
      </View>
      <View style={styles.containerBtnExpenseForm}>
        <Button title={t('save')} onPress={() => handleSubmit()} />
        <Text
          style={[mainStyles.text, styles.txtCancelExpenseForm]}
          onPress={() => onCancel()}>
          {t('cancel')}
        </Text>
      </View>
    </>
  );
};
