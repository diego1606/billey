import React, {useRef, useState} from 'react';
import {Platform, ScrollView, Text, View} from 'react-native';
import {mainStyles} from '../../../../assets';
import {Input} from 'react-native-elements';
import {useTranslation} from 'react-i18next';
import styles from './styles';
import i18n from '../../../../i18n';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import Button from '../../../../components/Button/Button';

export const CategoryForm = ({
  name,
  amount,
  keyText,
  currency,
  children,
  onSave,
}) => {
  const [nameCategory, setNameCategory] = useState(name);
  const [amountCategory, setAmountCategory] = useState(amount);
  const [errorName, setErrorName] = useState('');
  const [errorAmount, setErrorAmount] = useState('');
  const {t} = useTranslation();
  const nameInput = useRef(null);

  const handleOnSave = () => {
    let nErr = 0;
    if (nameCategory.length <= 0) {
      nErr++;
      setErrorName(t('required'));
    }
    if (amountCategory.length <= 0) {
      nErr++;
      setErrorAmount(t('required'));
    }
    if (nameCategory === i18n.t('new')) {
      nErr++;
      setErrorName(t('please_enter_another_category'));
    }
    if (nErr <= 0) {
      onSave({title: nameCategory, text: parseFloat(amountCategory), keyText});
    }
  };

  return (
    <ScrollView contentContainerStyle={mainStyles.f1}>
      <Text style={mainStyles.h1}>{nameCategory}</Text>
      <Input
        label={t('amount')}
        placeholder={t('eg_amount')}
        inputStyle={
          errorAmount.length <= 0 ? mainStyles.input : mainStyles.inputError
        }
        placeholderTextColor="#B0AFAF"
        labelStyle={mainStyles.inputLabel}
        inputContainerStyle={mainStyles.inputContainerStyle}
        autoCompleteType={'off'}
        autoCapitalize={'none'}
        value={amountCategory.toString()}
        onChangeText={(text) => {
          setAmountCategory(text);
        }}
        keyboardType="numeric"
        errorMessage={errorAmount}
        errorStyle={mainStyles.txtError}
        returnKeyType={'next'}
        onSubmitEditing={() => nameInput.current.focus()}
      />
      <Input
        ref={nameInput}
        label={t('name')}
        placeholder={t('eg_name')}
        inputStyle={
          errorName.length <= 0 ? mainStyles.input : mainStyles.inputError
        }
        labelStyle={mainStyles.inputLabel}
        inputContainerStyle={mainStyles.inputContainerStyle}
        autoCompleteType={'off'}
        autoCapitalize={'none'}
        value={nameCategory}
        onChangeText={(text) => {
          setNameCategory(text);
        }}
        placeholderTextColor="#B0AFAF"
        errorMessage={errorName}
        errorStyle={mainStyles.txtError}
        returnKeyType={'done'}
        onSubmitEditing={() => handleOnSave()}
      />
      <View style={styles.containerAmountEditCatForm}>
        <Text style={styles.amountEditCatForm}>
          {`${currency} ${amountCategory}`}
        </Text>
      </View>
      <View style={styles.containerBtnEditCatForm}>
        <Button
          title={t('save')}
          onPress={() => handleOnSave()}
          type={'secondary'}
          style={mainStyles.mb_2}
        />
        {children}
      </View>
      {Platform.OS === 'ios' ? <KeyboardSpacer /> : null}
    </ScrollView>
  );
};
