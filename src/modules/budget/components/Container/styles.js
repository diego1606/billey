import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  containerNewBud: {
    flex: 1,
    justifyContent: 'space-between',
    backgroundColor: 'white',
  },
  contentNewBud: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  textNewBud: {
    textAlign: 'center',
    marginTop: 30,
    marginBottom: 30,
  },
});
