import React, {useState} from 'react';
import {FlatList, Text, TouchableOpacity, View} from 'react-native';
import styles from './styles';
import {mainStyles} from '../../../../assets';

export const CurrencyInput = ({onClick, error = ''}) => {
  const [currency, setCurrency] = useState(null);
  const currencyAvailable = ['CHF', '€', '£'];

  const handleSelect = (cry) => {
    const index = currencyAvailable.indexOf(cry);
    if (index >= 0) {
      setCurrency(cry);
      onClick(cry);
    }
  };

  return (
    <View style={styles.containerCurrency}>
      {error.length <= 0 ? null : (
        <Text style={mainStyles.txtError}>{error}</Text>
      )}
      <FlatList
        numColumns={3}
        data={currencyAvailable}
        keyExtractor={(item) => item.toString()}
        renderItem={({item}) => {
          let styleCurrency = styles.currencyNormal;
          if (currency === item) {
            styleCurrency = styles.currencyActive;
          }
          return (
            <TouchableOpacity
              style={[styles.contentCurrency, styleCurrency]}
              onPress={() => handleSelect(item)}>
              <Text style={[styles.textCurrency, styleCurrency]}>{item}</Text>
            </TouchableOpacity>
          );
        }}
      />
    </View>
  );
};
