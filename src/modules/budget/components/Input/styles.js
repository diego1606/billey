import {StyleSheet} from 'react-native';
import {mainStyles} from '../../../../assets';

export default StyleSheet.create({
  containerCurrency: {
    alignItems: 'center',
    marginTop: 20,
    marginBottom: 20,
  },
  contentCurrency: {
    width: 100,
    height: 100,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    borderWidth: 1,
    margin: 10,
  },
  textCurrency: {
    fontSize: 25,
  },
  currencyNormal: {
    borderColor: '#e3e4e4',
  },
  currencyActive: {
    color: 'rgb(96, 145, 204)',
    borderColor: 'rgb(96, 145, 204)',
  },
  dateInput: {
    ...mainStyles.input,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    paddingLeft: 8,
    paddingTop: 15,
    height: 50,
  },
  dateInputError: {
    ...mainStyles.inputError,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    paddingLeft: 10,
    paddingTop: 10,
  },
  datePicker: {
    backgroundColor: '#393939',
  },
  dateInputComp: {
    paddingTop: 5,
  },
});
