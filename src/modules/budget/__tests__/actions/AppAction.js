import * as AppAction from '../../actions/AppAction';
import {CURRENCY_BUDGET, NAME_ACCOUNT} from '../../actions/types';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import BudgetRepository from '../../repositories/BudgetRepository';
import BudgetModel from '../../models/BudgetModel';

const middlewares = [thunk];
let mockStore = configureMockStore(middlewares);
let store;

jest.mock('react-native', () => {
  const RN = jest.requireActual('react-native');
  RN.NativeModules.SettingsManager = {
    settings: {
      AppleLocale: 'en_US',
    },
  };
  return RN;
});

describe('AppAction', () => {
  beforeEach(() => {
    store = mockStore();
  });

  it('should create an action to load the currency kind', () => {
    const currency = 'CHF';
    const expected = {
      type: CURRENCY_BUDGET,
      value: currency,
    };
    expect(AppAction.currencyBudget(currency)).toEqual(expected);
  });

  it('should create an action to load the name account', () => {
    const name = 'testAccount';
    const expected = {
      type: NAME_ACCOUNT,
      value: name,
    };
    expect(AppAction.nameAccount(name)).toEqual(expected);
  });

  it('should create the actions to load the necessary param for the app with the budget the same year', async () => {
    const startDate = '12-01-2020';
    const endDate = '12-02-2020';
    const name = 'testAccount';
    const currency = 'CHF';
    const titleForHome = 'Jan. - Feb. 2020';

    mockRepo(startDate, endDate);
    await assertLoadAppData(titleForHome, currency, name);
  });

  it('should create the actions to load the necessary param for the app with the budget the diff year', async () => {
    const startDate = '12-01-2020';
    const endDate = '12-02-2021';
    const name = 'testAccount';
    const currency = 'CHF';
    const titleForHome = 'Jan. - Feb. 2020/2021';

    mockRepo(startDate, endDate);
    await assertLoadAppData(titleForHome, currency, name);
  });
});

function mockRepo(startDate, endDate) {
  const mockGetAccount = jest.fn();
  jest.doMock('../../repositories/BudgetRepository');
  BudgetRepository.prototype.getBudget = mockGetAccount;
  mockGetAccount.mockReturnValue(new BudgetModel(startDate, endDate));
}
async function assertLoadAppData(titleForHome, currency, name) {
  const expected = [
    AppAction.titleHome(titleForHome),
    AppAction.currencyBudget(currency),
    AppAction.nameAccount(name),
  ];
  await store.dispatch(AppAction.loadAppData([{currency, name}]));
  expect(store.getActions()).toEqual(expected);
}
