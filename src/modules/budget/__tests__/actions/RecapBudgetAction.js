import thunk from 'redux-thunk';
import configureMockStore from 'redux-mock-store';
import BudgetRepository from '../../repositories/BudgetRepository';
import BudgetModel from '../../models/BudgetModel';
import CategoryModel from '../../models/CategoryModel';
import * as RecapBudgetAction from '../../actions/RecapBudgetAction';

jest.mock('react-native', () => {
  const RN = jest.requireActual('react-native');
  RN.NativeModules.SettingsManager = {
    settings: {
      AppleLocale: 'en_US',
    },
  };
  return RN;
});

const middlewares = [thunk];
let mockStore = configureMockStore(middlewares);
let store;

describe('RecapBudgetAction', () => {
  beforeEach(() => {
    store = mockStore({
      budgetReducer: {
        appReducer: {
          name: 'Test',
        },
      },
    });
  });
  it('should get the category and total money saved', async () => {
    const mockGetBudget = jest.fn();
    const mockFillBudgetWithCate = jest.fn();
    const budgetModel = new BudgetModel('12-02-2020', '12-03-2020');
    const categories = getCategoriesModel();
    categories.forEach((item) => {
      budgetModel.addCategory(item);
    });
    jest.doMock('../../repositories/BudgetRepository');
    BudgetRepository.prototype.getBudget = mockGetBudget;
    BudgetRepository.prototype.fillBudgetWithCategories = mockFillBudgetWithCate;
    mockGetBudget.mockReturnValue(budgetModel);
    mockFillBudgetWithCate.mockReturnThis();
    await store
      .dispatch(RecapBudgetAction.getBudgetWithExpense())
      .then((data) => {
        const {categories: cateData, total} = data;
        const categoriesView = budgetModel.categories.map((i) => i.toObjView());
        const moneySaved = categoriesView.reduce(
          (sum, item) => sum + parseFloat(item.text),
          0,
        );
        expect(cateData).toEqual(categoriesView);
        expect(total).toEqual(moneySaved);
      });
  });
});
function getCategoriesModel(): CategoryModel[] {
  const categoryModel = new CategoryModel('new', 'black');
  categoryModel.amount = 100;

  const categoryModel2 = new CategoryModel('new', 'black');
  categoryModel2.amount = 200;

  const categoryModel3 = new CategoryModel('new', 'black');
  categoryModel3.amount = 300;

  return [categoryModel, categoryModel2, categoryModel3];
}
