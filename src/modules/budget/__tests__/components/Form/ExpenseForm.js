import renderer from 'react-test-renderer';
import React from 'react';
import {ExpenseForm} from '../../../components/Form';
import {render, fireEvent} from 'react-native-testing-library';

jest.mock('react-i18next', () => ({
  useTranslation: () => ({
    t: (key) => key,
    i18n: {changeLanguage: jest.fn()},
  }),
}));

describe('ExpenseForm', () => {
  it('should render correctly the component', () => {
    const tree = renderer
      .create(
        <ExpenseForm
          currency={'CHF'}
          onSubmit={() => {}}
          dataPickerSelect={[]}
        />,
      )
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
  it('should not launch the event onSubmit when the input is empty', () => {
    const handleOnSubmit = jest.fn();
    const textClick = 'save';
    const {getByText} = render(
      <ExpenseForm onSubmit={handleOnSubmit} dataPickerSelect={[]} />,
    );
    fireEvent(getByText(textClick), 'onPress');
    expect(handleOnSubmit).toHaveBeenCalledTimes(0);
  });
  it('should not launch the event onSubmit when the input category is not selected', () => {
    const handleOnSubmit = jest.fn();
    const textClick = 'save';
    const {getByPlaceholder, getByText} = render(
      <ExpenseForm onSubmit={handleOnSubmit} dataPickerSelect={[]} />,
    );
    fireEvent(getByPlaceholder('0'), 'onChangeText', '10');
    fireEvent(getByText(textClick), 'onPress');
    expect(handleOnSubmit).toHaveBeenCalledTimes(0);
  });
});
