import renderer from 'react-test-renderer';
import {DateInput} from '../../../components/Input';
import React from 'react';
// eslint-disable-next-line no-unused-vars
import DateTimePickerModal from 'react-native-modal-datetime-picker';

jest.mock('react-native-modal-datetime-picker', () => 'DateTimePickerModal');

describe('DateInput', () => {
  it('should render correctly the component', () => {
    const tree = renderer
      .create(
        <DateInput label="test" date={'19-02-2020'} onChange={() => {}} />,
      )
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});
