import renderer from 'react-test-renderer';
import React from 'react';
import {CurrencyInput} from '../../../components/Input';

describe('CurrencyInput', () => {
  it('should render correctly the component', () => {
    const tree = renderer.create(<CurrencyInput onClick={() => {}} />).toJSON();
    expect(tree).toMatchSnapshot();
  });
  it('should show an error with the error props', () => {
    const tree = renderer
      .create(<CurrencyInput onClick={() => {}} error={'errorTest'} />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});
