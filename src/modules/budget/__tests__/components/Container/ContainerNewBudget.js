import renderer from 'react-test-renderer';
import React from 'react';
import ContainerNewBudget from '../../../components/Container/ContainerNewBudget';

jest.mock('../../../../../i18n', () => ({
  t: jest.fn(),
}));

describe('ContainerNewBudget', () => {
  it('should render correctly the component', () => {
    const tree = renderer.create(<ContainerNewBudget text={'Test'} />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
