import moment from 'moment';
import {globals} from '../../../assets';

export default class ExpenseModel {
  /** @var {datetime} created_at*/
  #created_at;
  /** @var {number} id*/
  #_id = -1;

  /**
   * @param {number} amount
   * @param {number} category_id
   * @param {number} budget_id
   * */
  constructor(amount: number, category_id: number, budget_id: number) {
    this.amount = amount;
    this.category_id = category_id;
    this.budget_id = budget_id;
    this.#created_at = moment().format(globals.FORMAT_DATETIME);
  }

  getAttrInArray() {
    return [this.amount, this.category_id, this.budget_id, this.#created_at];
  }

  get id(): number {
    return this.#_id;
  }

  set id(value: number) {
    this.#_id = value;
  }
}
