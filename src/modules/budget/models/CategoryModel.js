import moment from 'moment';
import {globals} from '../../../assets';
import i18n from '../../../i18n';

export default class CategoryModel {
  /** @var {datetime} created_at*/
  #created_at;
  /** @var {number} id*/
  #_id = -1;

  /** @var {number} _amount it's the amount define for the current category of the budget.*/
  #_amount = 0;

  /**
   * @param {string} key_text
   * @param {string} color
   * */
  constructor(key_text: string, color: string) {
    this.key_text = key_text;
    this.color = color;
    this.#created_at = moment().format(globals.FORMAT_DATETIME);
  }

  getAttrInArray() {
    return [this.key_text, this.color, this.#created_at];
  }

  toObjView(): Object {
    return {
      text: this.amount,
      title: i18n.t(this.key_text),
      keyText: this.key_text,
    };
  }

  get id(): number {
    return this.#_id;
  }

  set id(value: number) {
    this.#_id = value;
  }

  get amount(): number {
    return this.#_amount;
  }

  set amount(value: number) {
    this.#_amount = value;
  }
}
