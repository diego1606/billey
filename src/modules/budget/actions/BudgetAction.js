import i18n from '../../../i18n';
import moment from 'moment';
import {ErrorObj} from '../../../error/ErrorObj';
import {globals} from '../../../assets';
import {
  FIRST_CATEGORIES_BUDGET,
  FIRST_CURRENCY_BUDGET,
  FIRST_SAVE_BUDGET,
} from './types';
import CategoryRepository from '../repositories/CategoryRepository';
import AccountRepository from '../repositories/AccountRepository';
import BudgetCategoryRepository from '../repositories/BudgetCategoryRepository';
import BudgetRepository from '../repositories/BudgetRepository';
import {getColor} from './GraphAction';
import {fcmService} from '../../../services';
import BudgetModel from '../models/BudgetModel';
import AccountModel from '../models/AccountModel';
import {getAccounts} from './UserAction';
import ExpenseRepository from '../repositories/ExpenseRepository';

/**
 * @var name
 * @var typeReducer can take on one type of value : FIRST_NAME_BUDGET
 * */
export function setBudgetName(name, typeReducer) {
  return (dispatch) =>
    new Promise((resolve, reject) => {
      if (name.length <= 0) {
        reject(new Error(i18n.t('required')));
      } else if (name.length < 3) {
        reject(new Error(i18n.t('min_field_budget')));
      } else {
        dispatch(budgetName(typeReducer, name));
        resolve();
      }
    });
}
/**
 * @var typeReducer can take on two type of value : FIRST_DATE_BUDGET  or DATE_BUDGET
 * */
export function setDateBudget(startDate, endDate, typeReducer) {
  return (dispatch) =>
    new Promise((resolve, reject) => {
      const sDate = moment(startDate, globals.FORMAT_DATE);
      const eDate = moment(endDate, globals.FORMAT_DATE);
      let nError = 0;
      let errorObj = {
        startDate: '',
        endDate: '',
      };
      if (!sDate.isValid()) {
        nError++;
        errorObj.startDate = i18n.t('date_format');
      }
      if (!eDate.isValid()) {
        nError++;
        errorObj.endDate = i18n.t('date_format');
      }
      if (sDate >= eDate) {
        nError++;
        errorObj.startDate = i18n.t('date_incoherent');
        errorObj.endDate = i18n.t('date_incoherent');
      }
      if (nError <= 0) {
        dispatch(budgetDate(typeReducer, startDate, endDate));
        resolve();
      } else {
        reject(new ErrorObj(errorObj));
      }
    });
}

export function setCurrencyBudget(currency) {
  return (dispatch) =>
    new Promise((resolve, reject) => {
      if (currency.length <= 0) {
        reject(new Error(i18n.t('please_select_currency')));
      } else {
        dispatch(budgetCurrency(currency));
        resolve();
      }
    });
}

export function setCategoryBudget(item, type = FIRST_CATEGORIES_BUDGET) {
  return (dispatch, getState) =>
    new Promise((resolve) => {
      let categories = [];
      const {budgetReducer} = getState();
      if (type === FIRST_CATEGORIES_BUDGET) {
        categories = budgetReducer.firstBudgetReducer.categories;
      } else {
        categories = budgetReducer.infoBudgetReducer.categories;
      }
      const cloneCategories = JSON.parse(JSON.stringify(categories));
      let indexCategory = cloneCategories.findIndex(
        (i) => i.keyText === item.keyText,
      );
      if (indexCategory >= 0) {
        let itemCategory = cloneCategories[indexCategory];
        const titleTrans = i18n.t(itemCategory.keyText);
        itemCategory = {...item};
        if (item.title !== titleTrans) {
          itemCategory.keyText = item.title;
        }
        cloneCategories[indexCategory] = {...itemCategory};
        dispatch(budgetCategories(type, cloneCategories));
      }
      resolve();
    });
}

export function addCategoryBudget(item, type = FIRST_CATEGORIES_BUDGET) {
  return (dispatch, getState) =>
    new Promise((resolve) => {
      const {budgetReducer} = getState();
      let categories = [];
      if (type === FIRST_CATEGORIES_BUDGET) {
        categories = budgetReducer.firstBudgetReducer.categories;
      } else {
        categories = budgetReducer.infoBudgetReducer.categories;
      }
      const cloneCategories = JSON.parse(JSON.stringify(categories));
      const titleTrans = i18n.t(item.keyText);
      if (item.title !== titleTrans) {
        item.keyText = item.title;
      }
      cloneCategories.push(item);
      dispatch(budgetCategories(type, cloneCategories));
      resolve();
    });
}

export function removeOldBudget() {
  return new Promise(async (resolve) => {
    const budCatRepo = new BudgetCategoryRepository();
    budCatRepo.removeAllData();
    const expenseRepo = new ExpenseRepository();
    expenseRepo.removeAllData();
    const budgetRepo = new BudgetRepository();
    budgetRepo.removeAllData();
    const categoryRepo = new CategoryRepository();
    categoryRepo.removeAllData();
    resolve();
  });
}

export function saveLocalBudget(type) {
  return (dispatch, getState) =>
    new Promise(async (resolve, reject) => {
      const {budgetReducer} = getState();
      const {categories, name, startDate, endDate, currency} = await getArgs(
        type,
        budgetReducer,
      );
      const accountRepo = new AccountRepository();
      const budgetRepo = new BudgetRepository();
      const categoryRepo = new CategoryRepository();
      const budCatRepo = new BudgetCategoryRepository();
      let account: AccountModel;
      let budget: BudgetModel;
      if (type === FIRST_SAVE_BUDGET) {
        await accountRepo
          .insert({name, currency})
          .then((acc: AccountModel) => {
            account = acc;
          })
          .catch((e) => {
            reject(e);
          });
      } else {
        const accounts = await getAccounts();
        account = accounts[0];
      }
      if (account !== undefined) {
        await budgetRepo
          .insert({
            start_date: startDate,
            end_date: endDate,
            account_id: account.id,
          })
          .then((bu: BudgetModel) => {
            budget = bu;
          })
          .catch((e) => {
            reject(e);
          });
      }
      const categoriesWithColor = categories.map((item, i) => ({
        key_text: item.keyText,
        color: getColor(i),
        amount: item.text,
      }));
      let categoriesData = [];
      await Promise.all(
        categoriesWithColor.map((item) => categoryRepo.insertGetAmount(item)),
      )
        .then((c) => {
          categoriesData = c;
        })
        .catch(() => {
          reject(new Error(i18n.t('error_occurred_please_try_again')));
        });
      if (budget !== undefined && categoriesData.length > 0) {
        await Promise.all(
          categoriesData.map((item) =>
            budCatRepo.insert({
              amount: parseFloat(item.amount),
              category_id: item.category.id,
              budget_id: budget.id,
            }),
          ),
        )
          .then(() => {
            const datetimeStr = `${budget.end_date} ${globals.TIME_END_BUDGET}`;
            const date = moment(datetimeStr, globals.FORMAT_DATETIME).toDate();
            fcmService.scheduleNotification(
              i18n.t('budget_come_to_end_look_the_summery'),
              date,
            );
            resolve();
          })
          .catch(() => {
            reject(new Error(i18n.t('error_occurred_please_try_again')));
          });
      } else {
        reject(new Error(i18n.t('error_occurred_please_try_again')));
      }
    });
}
export async function getArgs(type, parentReducer): Object {
  let categories = [];
  let name: string = '';
  let startDate: string = '';
  let endDate: string = '';
  let currency: string = '';
  if (type === FIRST_SAVE_BUDGET) {
    categories = parentReducer.firstBudgetReducer.categories;
    name = parentReducer.firstBudgetReducer.name;
    startDate = parentReducer.firstBudgetReducer.startDate;
    endDate = parentReducer.firstBudgetReducer.endDate;
    currency = parentReducer.firstBudgetReducer.currency;
  } else {
    const accounts = await getAccounts();
    const account = accounts[0];
    categories = parentReducer.infoBudgetReducer.categories;
    startDate = parentReducer.infoBudgetReducer.startDate;
    endDate = parentReducer.infoBudgetReducer.endDate;
    currency = account.currency;
    name = account.name;
  }
  return {
    categories,
    name,
    startDate,
    endDate,
    currency,
  };
}

export function isEndBudget(): Promise {
  return new Promise(async (resolve) => {
    const repoBudget = new BudgetRepository();
    const budgetModel = await repoBudget.getBudget();
    const datetimeStr = `${budgetModel.end_date} ${globals.TIME_END_BUDGET}`;
    const date = moment(datetimeStr, globals.FORMAT_DATETIME);
    const now = moment();
    resolve(now.isAfter(date));
  });
}

export const budgetName = (type, name) => ({
  type,
  value: name,
});

export const budgetDate = (type, startDate, endDate) => ({
  type,
  value: {
    startDate,
    endDate,
  },
});

export const budgetCurrency = (currency) => ({
  type: FIRST_CURRENCY_BUDGET,
  value: currency,
});

export const budgetCategories = (type: string, categories: []) => ({
  type: type,
  value: categories,
});
