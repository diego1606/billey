import BudgetRepository from '../repositories/BudgetRepository';
import BudgetModel from '../models/BudgetModel';

export function getBudgetWithExpense() {
  return (dispatch, getState) =>
    new Promise(async (resolve) => {
      const repoBudget = new BudgetRepository();
      const {appReducer} = getState().budgetReducer;
      const {name} = appReducer;
      const budgetModel: BudgetModel = await repoBudget.getBudget({name});
      await repoBudget.fillBudgetWithCategories(budgetModel);
      const categoriesView = budgetModel.categories.map((i) => i.toObjView());
      const moneySaved = categoriesView.reduce(
        (sum, item) => sum + parseFloat(item.text),
        0,
      );
      resolve({categories: categoriesView, total: moneySaved});
    });
}
