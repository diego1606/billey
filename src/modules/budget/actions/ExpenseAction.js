import CategoryRepository from '../repositories/CategoryRepository';
import BudgetRepository from '../repositories/BudgetRepository';
import ExpenseRepository from '../repositories/ExpenseRepository';
import {DATA_BAR_GRAPH, EXPENSE, TOTAL_BUDGET} from './types';
import ExpenseModel from '../models/ExpenseModel';

export function saveExpense(amount, category) {
  return (dispatch, getState) =>
    new Promise(async (resolve, reject) => {
      const {appReducer, homeBudgetReducer} = getState().budgetReducer;
      const {name} = appReducer;
      const {total} = homeBudgetReducer;
      const repoCategory = new CategoryRepository();
      const repoBudget = new BudgetRepository();
      const repoExpense = new ExpenseRepository();
      const itemCategory = await repoCategory.getCategory({key_text: category});
      const itemBudget = await repoBudget.getBudget({name});
      repoExpense
        .insert({
          amount: parseFloat(amount),
          budget_id: itemBudget.id,
          category_id: itemCategory.id,
        })
        .then((expense) => {
          dispatch(updateTotal(total - amount));
          resolve(expense);
        })
        .catch((e) => {
          reject(e);
        });
    });
}

export function getAverageExpense() {
  return async (dispatch) => {
    const repo = new ExpenseRepository();
    dispatch(updateExpense(await repo.getAvgExpense()));
  };
}

export function updateDataWithExpense(expense: ExpenseModel) {
  return (dispatch, getState) => {
    const {homeBudgetReducer} = getState().budgetReducer;
    const dataBarGraph = JSON.parse(
      JSON.stringify(homeBudgetReducer.dataBarGraph),
    );
    const itemDecrease = dataBarGraph.find(
      (data) => data.category_id === expense.category_id,
    );
    if (itemDecrease !== undefined) {
      itemDecrease.y = itemDecrease.y - expense.amount;
    }
    dispatch(updateDataBarGraph(dataBarGraph));
  };
}

export const updateDataBarGraph = (data) => ({
  type: DATA_BAR_GRAPH,
  value: data,
});

export const updateExpense = (value) => ({
  type: EXPENSE,
  value,
});
export const updateTotal = (value) => ({
  type: TOTAL_BUDGET,
  value,
});
