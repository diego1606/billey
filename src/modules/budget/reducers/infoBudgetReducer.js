import {CATEGORIES_BUDGET, DATE_BUDGET} from '../actions/types';

const initState = {
  categories: [],
  startDate: '',
  endDate: '',
};

export function infoBudgetReducer(state = initState, action) {
  let nextState;
  const {type, value} = action;
  if (type === CATEGORIES_BUDGET) {
    nextState = {
      ...state,
      categories: value,
    };
    return nextState;
  }
  if (type === DATE_BUDGET) {
    nextState = {
      ...state,
      startDate: value.startDate,
      endDate: value.endDate,
    };
    return nextState;
  }
  return state;
}
