import {
  FIRST_CATEGORIES_BUDGET,
  FIRST_CURRENCY_BUDGET,
  FIRST_DATE_BUDGET,
  FIRST_NAME_BUDGET,
} from '../actions/types';
import categories from '../data/files/categories.json';
import i18n from '../../../../src/i18n';

const initState = {
  name: '',
  startDate: '',
  endDate: '',
  categories: categories.map((item) => ({
    text: item.amount,
    title: i18n.t(item.keyText),
    keyText: item.keyText,
  })),
  currency: '',
};

export function firstBudgetReducer(state = initState, action) {
  let nextState;
  const {type, value} = action;
  if (type === FIRST_NAME_BUDGET) {
    nextState = {
      ...state,
      name: value,
    };
    return nextState;
  }
  if (type === FIRST_DATE_BUDGET) {
    nextState = {
      ...state,
      startDate: value.startDate,
      endDate: value.endDate,
    };
    return nextState;
  }
  if (type === FIRST_CATEGORIES_BUDGET) {
    nextState = {
      ...state,
      categories: value,
    };
    return nextState;
  }
  if (type === FIRST_CURRENCY_BUDGET) {
    nextState = {
      ...state,
      currency: value,
    };
    return nextState;
  }
  return state;
}
