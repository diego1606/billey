import {DATA_PICKER_SELECT} from '../actions/types';

const initState = {
  dataPickerSelect: [],
};

export function inputReducer(state = initState, action) {
  let nextState;
  const {type, value} = action;
  if (type === DATA_PICKER_SELECT) {
    nextState = {
      ...state,
      dataPickerSelect: value,
    };
    return nextState;
  }
  return state;
}
