import {combineReducers} from 'redux';
import {firstBudgetReducer} from './reducers/firstBudgetReducer';
import {homeBudgetReducer} from './reducers/homeBudgetReducer';
import {inputReducer} from './reducers/inputReducer';
import {appReducer} from './reducers/appReducer';
import {infoBudgetReducer} from './reducers/infoBudgetReducer';

export const budgetReducer = combineReducers({
  firstBudgetReducer,
  homeBudgetReducer,
  inputReducer,
  appReducer,
  infoBudgetReducer,
});
