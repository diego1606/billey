import React from 'react';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {createStackNavigator, TransitionPresets} from '@react-navigation/stack';
import HomeBudgetView from '../views/HomeBudgetView';
import ExpenseBudgetView from '../views/ExpenseBudgetView';
import {MenuBudget} from '../../../components/Menu';
import OtherDateBudgetView from '../views/ForecastDateBudgetView';
import ForecastCategoryBudgetView from '../views/ForecastCategoryBudgetView';
import SaveForecastBudgetView from '../views/SaveForecastBudgetView';
import ForecastNewCategoryBudgetView from '../views/ForecastNewCategoryBudgetView';
import ForecastEditCategoryBudgetView from '../views/ForecastEditCategoryBudgetView';

import {
  BarsHeaderButton,
  PlusHeaderButton,
} from '../../../components/HeaderButton';
import RecapBudgetView from '../views/RecapBudgetView';

const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();
const headerWithoutBarTitle = {
  headerStyle: {
    borderBottomWidth: 0,
    elevation: 0,
    shadowOffset: {
      width: 0,
      height: 0,
    },
  },
  title: '',
};

const HomeBudgetNavigation = () => (
  <Stack.Navigator
    initialRouteName="Budget"
    screenOptions={{
      ...headerWithoutBarTitle,
    }}>
    <Stack.Screen
      name="Budget"
      component={HomeBudgetView}
      options={({navigation}) => ({
        headerLeft: () => (
          <BarsHeaderButton onPress={() => navigation.toggleDrawer()} />
        ),
        headerRight: () => (
          <PlusHeaderButton onPress={() => navigation.navigate('New_budget')} />
        ),
      })}
    />
    <Stack.Screen
      name={'Expense'}
      component={ExpenseBudgetView}
      options={{
        ...TransitionPresets.ModalTransition,
      }}
    />
    <Stack.Screen
      name={'New_budget'}
      component={OtherDateBudgetView}
      options={{
        ...TransitionPresets.ModalTransition,
      }}
    />
    <Stack.Screen
      name={'Overview'}
      component={RecapBudgetView}
      options={{
        ...TransitionPresets.ModalTransition,
      }}
    />
    <Stack.Screen
      name={'Forecast_category'}
      component={ForecastCategoryBudgetView}
      options={({navigation}) => ({
        headerRight: () => (
          <PlusHeaderButton
            onPress={() => navigation.navigate('Forecast_new_category')}
          />
        ),
      })}
    />
    <Stack.Screen name={'Forecast_save'} component={SaveForecastBudgetView} />
    <Stack.Screen
      name={'Forecast_new_category'}
      component={ForecastNewCategoryBudgetView}
      options={{
        ...TransitionPresets.ModalTransition,
      }}
    />
    <Stack.Screen
      name={'Forecast_edit_category'}
      component={ForecastEditCategoryBudgetView}
      options={{
        ...TransitionPresets.ModalTransition,
      }}
    />
  </Stack.Navigator>
);

export const HomeBudgetNavigationDraw = () => (
  <Drawer.Navigator
    initialRouteName="Home"
    drawerContent={(props) => <MenuBudget {...props} />}>
    <Drawer.Screen name="Home" component={HomeBudgetNavigation} />
  </Drawer.Navigator>
);
