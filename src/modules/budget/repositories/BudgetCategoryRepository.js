import {Repository} from './Repository';
import BudgetCategoryModel from '../models/BudgetCategoryModel';
import {db} from '../../../config';

export default class BudgetCategoryRepository extends Repository {
  insert(args: {}): Promise {
    this._sql =
      'INSERT INTO budgets_categories (amount, category_id, budget_id) VALUES(?,?,?)';
    return super.insert(args);
  }

  _createInstance(args) {
    this._model = new BudgetCategoryModel(
      args.amount,
      args.category_id,
      args.budget_id,
    );
  }

  getBudgetCategory() {
    this._sql = 'SELECT * FROM budgets_categories';
    return this.getItems();
  }

  removeAllData(): void {
    const sql = 'DELETE FROM budgets_categories';
    db.executeSql(sql).then(() => {});
  }
}
