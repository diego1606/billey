import BudgetModel from '../models/BudgetModel';
import {Repository} from './Repository';
import {db} from '../../../config';
import CategoryRepository from './CategoryRepository';
import ExpenseRepository from './ExpenseRepository';

export default class BudgetRepository extends Repository {
  insert(args: {}): Promise {
    this._sql =
      'INSERT INTO Budgets (account_id, start_date, end_date, created_at) VALUES(?,?,?,?)';
    return super.insert(args);
  }

  async fillBudgetWithCategories(budget: BudgetModel) {
    const sql =
      'SELECT budgets_categories.*, Categories.* FROM Budgets INNER JOIN budgets_categories ON budgets_categories.budget_id = Budgets.id ' +
      'INNER JOIN Categories ON budgets_categories.category_id = Categories.id WHERE Budget_id = ?';
    const param = [budget.id];
    await db.executeSql(sql, param).then(async (results) => {
      const size = results.rows.length;
      const repoCategory = new CategoryRepository();
      const repoExpense = new ExpenseRepository();
      for (let i = 0; i < size; i++) {
        const item = results.rows.item(i);
        const categoryInstance = repoCategory.getModelInstance(item);
        const amountExpenses = await repoExpense.getAmountExpenses(
          item.category_id,
          budget.id,
        );
        categoryInstance.amount = item.amount - amountExpenses;
        budget.addCategory(categoryInstance);
      }
    });
  }

  async getBudget(obj = {}): BudgetModel {
    let data: BudgetModel = null;
    const {id, name} = obj;
    let sql = '';
    const param = [];
    if (name !== undefined) {
      sql =
        'SELECT * FROM Budgets INNER JOIN Accounts ON accounts.id = budgets.account_id WHERE accounts.name=?';
      param.push(name);
    } else if (id !== undefined) {
      sql = 'SELECT * FROM Budgets WHERE id=?';
      param.push(id);
    } else {
      sql = 'SELECT * FROM Budgets LIMIT 1';
    }
    await db.executeSql(sql, param).then((result) => {
      if (result.rows.length > 0) {
        const item = result.rows.item(0);
        this._createInstance(item);
        data = this._model;
      }
    });
    return data;
  }

  _createInstance(args): Object {
    const model = new BudgetModel(args.start_date, args.end_date);
    model.account_id = args.account_id;
    if (args.hasOwnProperty('id')) {
      model.id = args.id;
    }
    this._model = model;
  }

  removeAllData(): void {
    const sql = 'DELETE FROM Budgets';
    db.executeSql(sql).then(() => {});
  }
}
