import {Repository} from './Repository';
import ExpenseModel from '../models/ExpenseModel';
import {db} from '../../../config';

export default class ExpenseRepository extends Repository {
  insert(args: {}): Promise {
    this._sql =
      'INSERT INTO Expenses (amount, category_id, budget_id, created_at) VALUES(?,?,?, ?)';
    return super.insert(args);
  }

  _createInstance(args) {
    const model = new ExpenseModel(
      args.amount,
      args.category_id,
      args.budget_id,
    );
    if (args.hasOwnProperty('id')) {
      model.id = args.id;
    }
    this._model = model;
  }

  async getAmountExpenses(categoryId: number, budgetId: number) {
    let amount = 0;
    if (Number.isInteger(categoryId) && Number.isInteger(budgetId)) {
      const sql =
        'SELECT SUM(amount) as amount FROM Expenses WHERE category_id=? AND budget_id=?';
      await db.executeSql(sql, [categoryId, budgetId]).then((results) => {
        amount = results.rows.item(0).amount;
      });
    }
    return amount;
  }
  async getAvgExpense(): number {
    const sql = 'SELECT AVG(amount) as avg FROM Expenses';
    let avg = 0;
    await db.executeSql(sql).then((result) => {
      if (result.rows.length > 0) {
        const resultAvg = result.rows.item(0).avg;
        if (resultAvg !== null) {
          avg = resultAvg.toFixed(2);
        }
      }
    });
    return avg;
  }

  removeAllData(): void {
    const sql = 'DELETE FROM Expenses';
    db.executeSql(sql).then(() => {});
  }
}
