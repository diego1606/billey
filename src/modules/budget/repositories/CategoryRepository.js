import CategoryModel from '../models/CategoryModel';
import {Repository} from './Repository';
import {db} from '../../../config';
import i18n from '../../../i18n';

export default class CategoryRepository extends Repository {
  async insertGetAmount(args: {}): Promise {
    const sqlUnique: string = 'SELECT * FROM Categories';
    let category: CategoryModel;
    await db.executeSql(sqlUnique).then((res) => {
      if (res.rows.length > 0) {
        const item = res.rows.item(0);
        category = new CategoryModel(item.key_text, item.color);
        category.id = item.id;
      }
    });
    if (category === undefined) {
      this._sql =
        'INSERT INTO Categories (key_text, color, created_at) VALUES(?,?,?)';
      await super
        .insert(args)
        .then((c) => {
          category = c;
        })
        .catch((e) => {
          Promise.reject(e);
        });
    }
    if (category !== undefined) {
      return Promise.resolve({category, amount: args.amount});
    } else {
      return Promise.reject(
        new Error(i18n.t('error_occurred_please_try_again')),
      );
    }
  }

  _createInstance(args): Object {
    const model = new CategoryModel(args.key_text, args.color);
    if (args.hasOwnProperty('id')) {
      model.id = args.id;
    }
    this._model = model;
  }

  async getCategories(): CategoryModel[] {
    this._sql = 'SELECT * FROM Categories';
    return this.getItems();
  }

  async getCategory(obj): CategoryModel {
    const {id, key_text} = obj;
    let sql = '';
    const params = [];
    if (id !== undefined) {
      sql = 'SELECT * FROM Categories WHERE id=?';
      params.push(id);
    } else if (key_text !== undefined) {
      sql = 'SELECT * FROM Categories WHERE key_text=?';
      params.push(key_text);
    }
    let data = null;
    if (id !== undefined || key_text !== undefined) {
      await db.executeSql(sql, params).then((results) => {
        const size = results.rows.length;
        if (size === 1) {
          const item = results.rows.item(0);
          this._createInstance(item);
          data = this._model;
        }
      });
    }
    return data;
  }
  removeAllData(): void {
    const sql = 'DELETE FROM Categories';
    db.executeSql(sql).then(() => {});
  }
}
