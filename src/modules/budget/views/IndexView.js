import React from 'react';
import ContainerNewBudget from '../components/Container/ContainerNewBudget';
import {ActivityIndicator, View} from 'react-native';
import {mainStyles} from '../../../assets';
import {getAccounts, loadAppData} from '../actions';
import {connect} from 'react-redux';

class IndexView extends React.Component {
  async componentDidMount() {
    const {navigation, dispatch} = this.props;
    const accounts = await getAccounts();
    if (accounts.length > 0) {
      dispatch(loadAppData(accounts)).then(() => {
        navigation.navigate('Home');
      });
    } else {
      navigation.navigate('First_new_budget');
    }
  }

  render() {
    return (
      <ContainerNewBudget>
        <View style={mainStyles.w100}>
          <ActivityIndicator size="large" />
        </View>
      </ContainerNewBudget>
    );
  }
}
export default connect(null)(IndexView);
