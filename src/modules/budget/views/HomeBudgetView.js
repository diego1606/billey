import React from 'react';
import {Text, View} from 'react-native';
import {withTranslation} from 'react-i18next';
import {mainStyles} from '../../../assets';
import {Button} from 'react-native-elements';
import styles from './styles';
import {BarGraph} from '../../../components/Graph';
import {getAverageExpense, getDataBarGraph, isEndBudget} from '../actions';
import {connect} from 'react-redux';
import {fcmService} from '../../../services';

class HomeBudgetView extends React.Component {
  state = {
    isLoading: true,
  };

  handleOnPress = () => {
    const {navigation} = this.props;
    navigation.navigate('Expense');
  };

  componentDidMount() {
    isEndBudget().then((isEnd) => {
      if (isEnd) {
        this.initWhenEndBudget();
      } else {
        this.init();
      }
    });
  }

  init(): void {
    const {dispatch} = this.props;
    dispatch(getDataBarGraph()).then(() => {
      this.setState({isLoading: false});
    });
    dispatch(getAverageExpense());
    fcmService.createNotificationListeners(
      this.onRegister,
      this.onMessageReceived,
    );
  }

  initWhenEndBudget(): void {
    this.setState({isLoading: false}, () => {
      const {navigation} = this.props;
      navigation.navigate('Overview');
    });
  }

  onRegister = (token) => {
    console.log('[Notification fcm ] onRegister:', token);
  };

  onMessageReceived = (messageObj) => {
    const {data} = messageObj;
    const {isBudgetEnd} = data;
    if (isBudgetEnd) {
      this.initWhenEndBudget();
    }
  };

  render() {
    const {t, total, currency, dataBarGraph, titleHome, expense} = this.props;
    const {isLoading} = this.state;
    return (
      <View style={mainStyles.container}>
        <View style={styles.containerTitleHome}>
          <Text style={styles.title_home}>{titleHome}</Text>
        </View>
        <View style={styles.containerGraphHome}>
          {/* FIXME Not necessary for the moment
            <GraphTab onPress={() => {}} />
          */}
          <BarGraph data={dataBarGraph} isLoading={isLoading} />
        </View>
        <View style={styles.containerTxtHome}>
          <Text style={[mainStyles.text, styles.textHomeBudget]}>
            {t('average_payment', {
              currency: currency,
              amount: expense,
            })}
          </Text>
          <Text style={[mainStyles.text, styles.textHomeBudget]}>
            {t('budget_total', {
              currency: currency,
              amount: total,
            })}
          </Text>
          <Button
            title={t('payment')}
            type="outline"
            onPress={this.handleOnPress}
            containerStyle={mainStyles.btn}
            titleStyle={mainStyles.white}
            buttonStyle={styles.btnStyleHome}
          />
        </View>
      </View>
    );
  }
}
const mapStateToProps = (state) => {
  const {homeBudgetReducer, appReducer} = state.budgetReducer;
  return {
    total: homeBudgetReducer.total,
    currency: appReducer.currency,
    titleHome: appReducer.titleHome,
    dataBarGraph: homeBudgetReducer.dataBarGraph,
    expense: homeBudgetReducer.expense,
  };
};

export default withTranslation()(connect(mapStateToProps)(HomeBudgetView));
