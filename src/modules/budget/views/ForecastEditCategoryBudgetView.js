import React from 'react';
import {Text, View} from 'react-native';
import {mainStyles} from '../../../assets';
import {withTranslation} from 'react-i18next';
import {connect} from 'react-redux';
import {deleteCategory, setCategoryBudget} from '../actions';
import {CATEGORIES_BUDGET} from '../actions/types';
import {CategoryForm} from '../components/Form';

class ForecastEditCategoryBudgetView extends React.Component {
  handleOnSave = (category) => {
    const {dispatch, navigation} = this.props;
    dispatch(setCategoryBudget(category, CATEGORIES_BUDGET)).then(() => {
      navigation.goBack();
    });
  };

  handleOnDelete = (category) => {
    const {dispatch, navigation} = this.props;
    dispatch(deleteCategory(category, CATEGORIES_BUDGET)).then((_) => {
      navigation.goBack();
    });
  };

  render() {
    const {route, t} = this.props;
    const {category, currency} = route.params;
    return (
      <View style={mainStyles.container}>
        <CategoryForm
          name={category.title}
          amount={category.text}
          keyText={category.keyText}
          currency={currency}
          onSave={(c) => this.handleOnSave(c)}>
          <Text
            style={[mainStyles.textAlignCenter, mainStyles.text]}
            onPress={() => this.handleOnDelete(category)}>
            {t('delete')}
          </Text>
        </CategoryForm>
      </View>
    );
  }
}

export default withTranslation()(connect(null)(ForecastEditCategoryBudgetView));
