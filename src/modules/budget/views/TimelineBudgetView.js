import React from 'react';
import {withTranslation} from 'react-i18next';
import ContainerNewBudget from '../components/Container/ContainerNewBudget';
import {DateInput} from '../components/Input';
import Space from '../../../components/Space/Space';
import {globals, mainStyles} from '../../../assets';
import moment from 'moment';
import {connect} from 'react-redux';
import {setDateBudget} from '../actions';
import {FIRST_DATE_BUDGET} from '../actions/types';
import {View} from 'react-native';
import {Button} from '../../../components/Button';

class TimelineBudgetView extends React.Component {
  state = {
    startDate: moment().format(globals.FORMAT_DATE),
    endDate: moment().add(1, 'month').format(globals.FORMAT_DATE),
    startDateError: '',
    endDateError: '',
  };
  handleOnClick = () => {
    const {navigation, dispatch} = this.props;
    const {startDate, endDate} = this.state;
    dispatch(setDateBudget(startDate, endDate, FIRST_DATE_BUDGET))
      .then(() => {
        this.setState(
          {
            startDateError: '',
            endDateError: '',
          },
          () => {
            navigation.navigate('Currency');
          },
        );
      })
      .catch((eObj) => {
        const {startDate: start, endDate: end} = eObj.message;
        this.setState({
          startDateError: start,
          endDateError: end,
        });
      });
  };

  handleOnChangeStartDate = (startDate) => {
    this.setState({startDate});
  };
  handleOnChangeEndDate = (endDate) => {
    this.setState({endDate});
  };

  render() {
    const {t} = this.props;
    const {startDate, endDate, startDateError, endDateError} = this.state;
    return (
      <ContainerNewBudget text={t('define_your_budget_duration')}>
        <View style={[mainStyles.ph_1, mainStyles.w100]}>
          <DateInput
            label={t('start_date')}
            date={startDate}
            onChange={this.handleOnChangeStartDate}
            error={startDateError}
          />
          <Space height={10} />
          <DateInput
            label={t('end_date')}
            date={endDate}
            onChange={this.handleOnChangeEndDate}
            error={endDateError}
          />
          <View style={mainStyles.pt_3}>
            <Button
              title={t('next')}
              onPress={this.handleOnClick}
              type={'secondary'}
            />
          </View>
        </View>
      </ContainerNewBudget>
    );
  }
}
export default withTranslation()(connect(null)(TimelineBudgetView));
