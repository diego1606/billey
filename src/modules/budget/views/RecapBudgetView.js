import React from 'react';
import {Text, View} from 'react-native';
import styles from './styles';
import {withTranslation} from 'react-i18next';
import {connect} from 'react-redux';
import {mainStyles} from '../../../assets';
import {SquareList} from '../../../components/List';
import {Button} from '../../../components/Button';
import {getBudgetWithExpense} from '../actions';

class RecapBudgetView extends React.Component {
  state = {
    categories: [],
    total: 0,
  };

  componentDidMount() {
    const {dispatch} = this.props;
    dispatch(getBudgetWithExpense()).then((data) => {
      const {categories, total} = data;
      this.setState({categories, total});
    });
  }

  getStyleForBudget(amount) {
    if (amount > 0) {
      return mainStyles.txtSuccess;
    }
    return mainStyles.txtError;
  }
  navigateTo(route): void {
    const {navigation} = this.props;
    navigation.navigate(route);
  }

  render() {
    const {categories, total} = this.state;
    const {t, period, currency} = this.props;
    return (
      <View style={mainStyles.container}>
        <View style={styles.containerTitleHome}>
          <Text style={styles.title_home}>{t('overview')}</Text>
          <Text style={mainStyles.text}>
            {t('budget_for_period', {period})}
          </Text>
        </View>
        <View style={styles.containerOverviewCategory}>
          <SquareList data={categories} onPress={(item) => {}} />
        </View>
        <View style={styles.containerOverviewSousTitle}>
          <Text
            style={[
              styles.txtSavingMoneyAmount,
              {...this.getStyleForBudget(total)},
            ]}>
            {t('save_money_amount', {
              currency: currency,
              amount: total,
            })}
          </Text>
          <Text style={styles.txtSavingMoneySousTitle}>
            {t('i_been_saving_up')}
          </Text>
          <Button
            title={t('define_new_budget')}
            onPress={() => this.navigateTo('New_budget')}
            type={'secondary'}
          />
        </View>
      </View>
    );
  }
}
const mapStateToProps = (state) => {
  const {appReducer} = state.budgetReducer;
  return {
    period: appReducer.titleHome,
    currency: appReducer.currency,
  };
};
export default withTranslation()(connect(mapStateToProps)(RecapBudgetView));
