import {withTranslation} from 'react-i18next';
import ContainerNewBudget from '../components/Container/ContainerNewBudget';
import React from 'react';
import {View} from 'react-native';
import {CurrencyInput} from '../components/Input';
import {mainStyles} from '../../../assets';
import {setCurrencyBudget} from '../actions';
import {connect} from 'react-redux';
import {Button} from '../../../components/Button';

class CurrencyBudgetView extends React.Component {
  state = {
    currency: '',
    error: '',
  };
  handleOnClick = () => {
    const {navigation, dispatch} = this.props;
    const {currency} = this.state;
    dispatch(setCurrencyBudget(currency))
      .then(() => {
        this.setState(
          {
            error: '',
          },
          () => {
            navigation.navigate('Category');
          },
        );
      })
      .catch((e) => {
        this.setState({
          error: e.message,
        });
      });
  };

  render() {
    const {t} = this.props;
    const {error} = this.state;
    return (
      <ContainerNewBudget text={t('what_your_currency')}>
        <View style={mainStyles.w100}>
          <CurrencyInput
            onClick={(currency) => this.setState({currency})}
            error={error}
          />
          <View style={[mainStyles.ph_1]}>
            <Button
              title={t('next')}
              onPress={this.handleOnClick}
              type={'secondary'}
            />
          </View>
        </View>
      </ContainerNewBudget>
    );
  }
}
export default withTranslation()(connect(null)(CurrencyBudgetView));
