import {StyleSheet} from 'react-native';
import {mainStyles} from '../../../assets';

export default StyleSheet.create({
  buttonContainerNewBud: {
    width: '88%',
    marginTop: 30,
    marginBottom: 30,
  },
  containerTitleCategoryBud: {
    flex: 0.6,
  },
  containerListBud: {
    flex: 2,
    alignItems: 'center',
  },
  containerTotalBud: {
    flex: 0.8,
    paddingTop: 30,
    alignItems: 'center',
  },
  textCategoryBud: {
    color: 'black',
    fontSize: 16,
    marginTop: 20,
  },
  textTotalCategoryBud: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  textHomeBudget: {
    fontSize: 20,
    marginBottom: 20,
  },
  containerOtherDateBudget: {
    flex: 2,
    backgroundColor: 'white',
    paddingHorizontal: 20,
  },
  containerInputOtherDateBudget: {
    flex: 3,
    alignItems: 'center',
  },
  containerCheckBoxSaveForecast: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  txtCheckBox: {
    paddingLeft: 10,
    fontSize: 17,
    paddingRight: 20,
  },
  btnStyleHome: {
    borderColor: 'transparent',
    marginTop: 10,
    backgroundColor: '#494949',
  },
  containerCreditFDB: {
    flex: 0.5,
  },
  btnCancelFDB: {
    marginTop: 20,
  },
  btnNextFDB: {
    marginTop: 30,
    width: '100%',
  },
  titleFDB: {
    marginTop: 15,
    marginBottom: 30,
  },
  containerTitleHome: {
    flex: 0.5,
    paddingTop: 10,
  },
  containerGraphHome: {
    flex: 2,
  },
  containerTxtHome: {
    flex: 1,
    paddingTop: 50,
  },
  containerBtnSaveForecast: {
    flex: 1,
    paddingTop: 50,
  },
  btnSaveForecast: {
    alignSelf: 'center',
  },
  title_home: {
    ...mainStyles.h1,
    fontSize: 33,
  },
  containerOverviewCategory: {
    ...mainStyles.f2,
    alignItems: 'center',
  },
  containerOverviewSousTitle: {
    ...mainStyles.f1,
    paddingTop: 20,
  },
  txtSavingMoneyAmount: {
    ...mainStyles.textAlignCenter,
    fontSize: 30,
  },
  txtSavingMoneySousTitle: {
    ...mainStyles.textAlignCenter,
    marginBottom: 20,
    fontSize: 12,
  },
});
