import React from 'react';
import {View} from 'react-native';
import {withTranslation} from 'react-i18next';
import {mainStyles} from '../../../assets';
import {Input} from 'react-native-elements';
import ContainerNewBudget from '../components/Container/ContainerNewBudget';
import {connect} from 'react-redux';
import {setBudgetName} from '../actions';
import {FIRST_NAME_BUDGET} from '../actions/types';
import {Button} from '../../../components/Button';

class NameBudgetView extends React.Component {
  state = {
    name: '',
    nameError: '',
  };

  handleOnTextChange = (t) => {
    if (!/\s/.test(t)) {
      this.setState({name: t});
    }
  };

  handleOnClick = () => {
    const {name} = this.state;
    const {navigation, dispatch} = this.props;
    dispatch(setBudgetName(name, FIRST_NAME_BUDGET))
      .then(() => {
        this.setState(
          {
            nameError: '',
          },
          () => {
            navigation.navigate('Timeline');
          },
        );
      })
      .catch((e) => {
        this.setState({
          nameError: e.message,
        });
      });
  };

  render() {
    const {t} = this.props;
    const {name, nameError} = this.state;
    return (
      <ContainerNewBudget text={t('welcome_to_the_app')}>
        <View style={mainStyles.w100}>
          <Input
            label={t('budget_name')}
            placeholder={t('eg_budget_name')}
            inputStyle={
              nameError.length <= 0 ? mainStyles.input : mainStyles.inputError
            }
            placeholderTextColor="#B0AFAF"
            labelStyle={mainStyles.inputLabel}
            inputContainerStyle={mainStyles.inputContainerStyle}
            autoCompleteType={'off'}
            autoCapitalize={'none'}
            value={name}
            errorMessage={nameError}
            errorStyle={mainStyles.txtError}
            onChangeText={(text) => this.handleOnTextChange(text)}
            returnKeyType={'next'}
            onSubmitEditing={() => this.handleOnClick()}
            autoCorrect={false}
          />
          <View style={mainStyles.ph_1}>
            <Button
              title={t('next')}
              onPress={this.handleOnClick}
              type={'secondary'}
            />
          </View>
        </View>
      </ContainerNewBudget>
    );
  }
}

export default withTranslation()(connect(null)(NameBudgetView));
