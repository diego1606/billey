import React from 'react';
import {DateInput} from '../components/Input';
import {Text, View} from 'react-native';
import styles from './styles';
import {withTranslation} from 'react-i18next';
import {globals, mainStyles} from '../../../assets';
import DesignByGT from '../../../components/Brand/DesignByGT';
import Space from '../../../components/Space/Space';
import moment from 'moment';
import {connect} from 'react-redux';
import {setDateBudget} from '../actions';
import {DATE_BUDGET} from '../actions/types';
import Button from '../../../components/Button/Button';

class ForecastDateBudgetView extends React.Component {
  state = {
    startDate: moment().format(globals.FORMAT_DATE),
    endDate: moment().add(1, 'month').format(globals.FORMAT_DATE),
    startDateError: '',
    endDateError: '',
  };

  handleOnClick = () => {
    const {navigation, dispatch} = this.props;
    const {startDate, endDate} = this.state;
    dispatch(setDateBudget(startDate, endDate, DATE_BUDGET))
      .then(() => {
        this.setState(
          {
            startDateError: '',
            endDateError: '',
          },
          () => {
            navigation.navigate('Forecast_category');
          },
        );
      })
      .catch((eObj) => {
        const {startDate: start, endDate: end} = eObj.message;
        this.setState({
          startDateError: start,
          endDateError: end,
        });
      });
  };
  handleOnChangeStartDate = (startDate) => {
    this.setState({startDate});
  };
  handleOnChangeEndDate = (endDate) => {
    this.setState({endDate});
  };
  render() {
    const {startDate, endDate, startDateError, endDateError} = this.state;
    const {t, navigation} = this.props;
    return (
      <View style={styles.containerOtherDateBudget}>
        <View style={mainStyles.f2}>
          <Text style={mainStyles.h1}>{t('new_forecast')}</Text>
          <Text style={[mainStyles.text, styles.titleFDB]}>
            {t('add_date_for_new_forecast')}
          </Text>
        </View>
        <View style={styles.containerInputOtherDateBudget}>
          <DateInput
            label={t('start_date')}
            width={'100%'}
            date={startDate}
            onChange={this.handleOnChangeStartDate}
            error={startDateError}
          />
          <Space height={10} />
          <DateInput
            label={t('end_date')}
            width={'100%'}
            date={endDate}
            onChange={this.handleOnChangeEndDate}
            error={endDateError}
          />
          <View style={[mainStyles.pt_3, mainStyles.w100]}>
            <Button
              title={t('next')}
              onPress={this.handleOnClick}
              type={'secondary'}
            />
          </View>
          <Text
            style={[styles.btnCancelFDB, mainStyles.text]}
            onPress={() => navigation.goBack()}>
            {t('cancel')}
          </Text>
        </View>
        <View style={styles.containerCreditFDB}>
          <DesignByGT />
        </View>
      </View>
    );
  }
}
export default withTranslation()(connect(null)(ForecastDateBudgetView));
