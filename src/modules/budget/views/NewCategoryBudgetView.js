import React from 'react';
import {Text, View} from 'react-native';
import {mainStyles} from '../../../assets';
import {withTranslation} from 'react-i18next';
import {connect} from 'react-redux';
import {addCategoryBudget} from '../actions';
import {CategoryForm} from '../components/Form';

class NewCategoryBudgetView extends React.Component {
  handleOnSave = (category) => {
    const {dispatch, navigation} = this.props;
    dispatch(addCategoryBudget(category)).then(() => {
      navigation.goBack();
    });
  };

  render() {
    const {t, currency, navigation} = this.props;
    return (
      <View style={mainStyles.container}>
        <CategoryForm
          name={t('new')}
          amount={'0'}
          keyText={'new'}
          currency={currency}
          onSave={this.handleOnSave}>
          <Text
            style={[mainStyles.textAlignCenter, mainStyles.text]}
            onPress={() => navigation.goBack()}>
            {t('cancel')}
          </Text>
        </CategoryForm>
      </View>
    );
  }
}
const mapStateToProps = (state) => {
  const {firstBudgetReducer} = state.budgetReducer;
  return {
    currency: firstBudgetReducer.currency,
  };
};
export default withTranslation()(
  connect(mapStateToProps)(NewCategoryBudgetView),
);
