import React from 'react';
import {Text, View} from 'react-native';
import {withTranslation} from 'react-i18next';
import {SquareList} from '../../../components/List';
import {mainStyles} from '../../../assets';
import styles from './styles';
import {connect} from 'react-redux';
import {saveLocalBudget} from '../actions';
import {fcmService} from '../../../services';
import {Button} from '../../../components/Button';
import {CommonActions} from '@react-navigation/native';
import {FIRST_SAVE_BUDGET} from '../actions/types';

class CategoryBudgetView extends React.Component {
  handleOnPressItem = (item) => {
    const {navigation} = this.props;
    navigation.navigate('Edit_category', {
      category: item,
    });
  };

  getTotal(categories) {
    return categories.reduce((sum, item) => sum + parseFloat(item.text), 0);
  }

  handleOnSave = () => {
    const {dispatch, navigation} = this.props;
    dispatch(saveLocalBudget(FIRST_SAVE_BUDGET)).then(async () => {
      await fcmService.checkPermissionFCM();
      const resetAction = CommonActions.reset({
        index: 1,
        routes: [{name: 'Index'}],
      });
      navigation.dispatch(resetAction);
    });
  };

  render() {
    const {t, categories, currency} = this.props;
    const total = this.getTotal(categories);
    return (
      <View style={mainStyles.container}>
        <View style={styles.containerTitleCategoryBud}>
          <Text style={mainStyles.h1}>{t('app_name')}</Text>
          <Text style={[mainStyles.text, styles.textCategoryBud]}>
            {t('define_your_budget')}
          </Text>
        </View>
        <View style={styles.containerListBud}>
          <SquareList
            data={categories}
            onPress={(item) => this.handleOnPressItem(item)}
          />
        </View>
        <View style={styles.containerTotalBud}>
          <Text style={styles.textTotalCategoryBud}>
            {t('total', {currency, amount: total})}
          </Text>
          <View style={mainStyles.w100}>
            <Button title={t('save')} onPress={this.handleOnSave} />
          </View>
        </View>
      </View>
    );
  }
}
const mapStateToProps = (state) => {
  const {firstBudgetReducer} = state.budgetReducer;
  return {
    categories: firstBudgetReducer.categories,
    currency: firstBudgetReducer.currency,
  };
};
export default withTranslation()(connect(mapStateToProps)(CategoryBudgetView));
