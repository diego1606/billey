import React from 'react';
import {withTranslation} from 'react-i18next';
import styles from './styles';
import {ActivityIndicator, Text, View} from 'react-native';
import {mainStyles} from '../../../assets';
import {SquareList} from '../../../components/List';
import {Button} from 'react-native-elements';
import {getAccounts, getCategories} from '../actions';
import {connect} from 'react-redux';

class ForecastCategoryBudgetView extends React.Component {
  state = {
    isLoading: true,
    currency: '',
  };
  async componentDidMount() {
    const {dispatch} = this.props;
    await dispatch(getCategories());
    const accounts = await getAccounts();
    const currency = accounts[0].currency;
    // eslint-disable-next-line react/no-did-mount-set-state
    this.setState({currency, isLoading: false});
  }

  handleNext = () => {
    const {navigation} = this.props;
    navigation.navigate('Forecast_save');
  };

  handleOnPressItem = (item) => {
    const {currency} = this.state;
    const {navigation} = this.props;
    navigation.navigate('Forecast_edit_category', {
      category: item,
      currency,
    });
  };

  getTotal(categories) {
    return categories.reduce((sum, item) => sum + parseFloat(item.text), 0);
  }

  renderContent() {
    const {isLoading, currency} = this.state;
    const {t, categories} = this.props;
    if (isLoading) {
      return <ActivityIndicator size="large" />;
    }
    const total = this.getTotal(categories);
    return (
      <>
        <View style={styles.containerListBud}>
          <SquareList
            data={categories}
            onPress={(item) => this.handleOnPressItem(item)}
          />
        </View>
        <View style={styles.containerTotalBud}>
          <Text style={styles.textTotalCategoryBud}>
            {t('total', {currency, amount: total})}
          </Text>
          <Button
            title={t('next')}
            type="outline"
            containerStyle={[
              styles.buttonContainerNewBud,
              mainStyles.textAlignCenter,
            ]}
            onPress={() => this.handleNext()}
          />
        </View>
      </>
    );
  }
  render() {
    const {t} = this.props;
    return (
      <View style={mainStyles.container}>
        <View style={styles.containerTitleCategoryBud}>
          <Text style={mainStyles.h1}>{t('app_name')}</Text>
          <Text style={[mainStyles.text, styles.textCategoryBud]}>
            {t('define_your_budget')}
          </Text>
        </View>
        {this.renderContent()}
      </View>
    );
  }
}
const mapStateToProps = (state) => {
  const {infoBudgetReducer} = state.budgetReducer;
  return {
    categories: infoBudgetReducer.categories,
  };
};
export default withTranslation()(
  connect(mapStateToProps)(ForecastCategoryBudgetView),
);
