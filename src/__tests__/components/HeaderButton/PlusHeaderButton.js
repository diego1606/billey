import renderer from 'react-test-renderer';
import PlusHeaderButton from '../../../components/HeaderButton/PlusHeaderButton';
import React from 'react';

describe('PlusHeaderButton', () => {
  it('should render correctly component', () => {
    const tree = renderer
      .create(<PlusHeaderButton onPress={() => {}} />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});
