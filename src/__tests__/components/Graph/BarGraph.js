import React from 'react';
import {BarGraph} from '../../../components/Graph/BarGraph';
import renderer from 'react-test-renderer';

describe('BarGraph', () => {
  it('should load the component', () => {
    const tree = renderer
      .create(<BarGraph isLoading={true} data={[]} />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
  it('should render correctly component', () => {
    const data = [
      {
        x: 'test',
        y: 12,
      },
      {
        x: 'test',
        y: 13,
      },
    ];
    const tree = renderer.create(<BarGraph data={data} />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
