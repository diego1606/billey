import DesignByGT from '../../../components/Brand/DesignByGT';
import React from 'react';
import renderer from 'react-test-renderer';

jest.mock('react-native', () => {
  const RN = jest.requireActual('react-native');
  RN.NativeModules.SettingsManager = {
    settings: {
      AppleLocale: 'en_US',
    },
  };
  return RN;
});

describe('DesignByGT', () => {
  it('should render correctly component', () => {
    const tree = renderer.create(<DesignByGT />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
