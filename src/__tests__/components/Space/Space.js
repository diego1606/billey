import Space from '../../../components/Space/Space';
import React from 'react';
import renderer from 'react-test-renderer';

describe('Space', () => {
  it('should render correctly the component', () => {
    const tree = renderer.create(<Space height={10} />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
