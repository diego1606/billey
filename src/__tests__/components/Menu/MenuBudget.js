import React from 'react';
import renderer from 'react-test-renderer';
// eslint-disable-next-line no-unused-vars
import DesignByGT from '../../../components/Brand/DesignByGT';
import {render, fireEvent} from 'react-native-testing-library';
import {MenuBudget} from '../../../components/Menu/MenuBudget';

jest.mock('react-i18next', () => ({
  useTranslation: () => ({
    t: (key) => key,
    i18n: {changeLanguage: jest.fn()},
  }),
}));

jest.mock('../../../__mocks__/react-native-reanimated');
jest.mock('../../../components/Brand/DesignByGT', () => 'DesignByGT');
jest.mock('react-native-safe-area-context', () => ({
  useSafeArea: () => ({insets: null}),
}));

const handleNavigation = {
  navigate: jest.fn(),
};

describe('MenuBudget', () => {
  it('should render correctly the component', () => {
    const tree = renderer
      .create(<MenuBudget navigation={handleNavigation} />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
  it('should navigate the route « Home » ', () => {
    const textClick = 'Mon_budget';
    const expected = 'Home';
    const {getByText} = render(
      <MenuBudget navigation={handleNavigation} name={textClick} />,
    );
    fireEvent(getByText(`⚖️ ${textClick}`), 'onPress');
    expect(handleNavigation.navigate).toHaveBeenCalledWith(expected);
  });
});
