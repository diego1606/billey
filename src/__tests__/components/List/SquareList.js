import renderer from 'react-test-renderer';
import React from 'react';
import {SquareList} from '../../../components/List';
import {render, fireEvent} from 'react-native-testing-library';

const data = [
  {
    text: 'Test',
    title: 'titleTest',
  },
  {
    text: 'Test2',
    title: 'titleTest2',
  },
  {
    text: 'Test22341',
    title: 'titleTest2',
  },
];
describe('SquareList', () => {
  it('should render correctly the component', () => {
    const tree = renderer
      .create(<SquareList data={data} onPress={() => {}} />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
  it('should get the data when the user click a item', () => {
    const handleOnPress = jest.fn();
    const expected = data[0];
    const {getByText} = render(
      <SquareList data={data} onPress={handleOnPress} />,
    );
    fireEvent(getByText(expected.text), 'onPress');
    expect(handleOnPress).toHaveBeenCalledWith(expected);
  });
});
