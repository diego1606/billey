import renderer from 'react-test-renderer';
import React from 'react';
import {GraphTab} from '../../../components/Tab';
import {render, fireEvent} from 'react-native-testing-library';

describe('GraphTab', () => {
  it('should render correctly the component', () => {
    const tree = renderer.create(<GraphTab onPress={() => {}} />).toJSON();
    expect(tree).toMatchSnapshot();
  });
  it('should get a item when the user click a icon', () => {
    const handleOnPress = jest.fn();
    const expected = {
      name: 'chart-bar',
    };
    const {getByTestId} = render(<GraphTab onPress={handleOnPress} />);
    fireEvent(getByTestId(expected.name), 'onPress');
    expect(handleOnPress).toHaveBeenCalledWith(expected);
  });
});
