import renderer from 'react-test-renderer';
import Button from '../../../components/Button/Button';
import React from 'react';

const title = 'btnTitle';
describe('Button', () => {
  it('should render correctly component with the primary type', () => {
    const tree = renderer
      .create(<Button title={title} onPress={() => {}} />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
  it('should render correctly component with the secondary type ', () => {
    const tree = renderer
      .create(<Button title={title} type={'secondary'} onPress={() => {}} />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
  it('should render correctly component with the other type ', () => {
    const tree = renderer
      .create(<Button title={title} type={'testType'} onPress={() => {}} />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});
