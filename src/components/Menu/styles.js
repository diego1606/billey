import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    paddingLeft: 10,
    paddingTop: 30,
  },
  containerTitle: {
    flex: 1,
  },
  containerItem: {
    height: '100%',
    marginBottom: 10,
  },
  containerCredit: {
    height: '100%',
    justifyContent: 'flex-end',
  },
  txtNameBudget: {
    marginTop: 40,
    marginBottom: 40,
    fontSize: 25,
    marginLeft: 10,
  },
  txtItemMenu: {
    marginBottom: 30,
    fontSize: 20,
    marginLeft: 10,
  },
});
