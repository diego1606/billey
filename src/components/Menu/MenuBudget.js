import React from 'react';
import {Text, View} from 'react-native';
import {useTranslation} from 'react-i18next';
import {DrawerContentScrollView} from '@react-navigation/drawer';
import {mainStyles} from '../../assets';
import DesignByGT from '../Brand/DesignByGT';
import styles from './styles';
import {connect} from 'react-redux';

export const MenuBudget = ({navigation, name, ...props}) => {
  const {t} = useTranslation();

  const handleNavigation = (route) => {
    navigation.navigate(route);
  };
  return (
    <DrawerContentScrollView style={styles.container} {...props}>
      <View style={styles.containerTitle}>
        <Text style={mainStyles.h1}>{t('app_name')}</Text>
      </View>
      <View style={styles.containerItem}>
        <Text
          style={styles.txtNameBudget}
          onPress={() => handleNavigation('Home')}>
          ⚖️ {name}
        </Text>
      </View>
      <View style={styles.containerCredit}>
        <DesignByGT />
      </View>
    </DrawerContentScrollView>
  );
};
const mapStateToProps = (state) => {
  const {appReducer} = state.budgetReducer;
  return {
    name: appReducer.name,
  };
};
export default connect(mapStateToProps)(MenuBudget);
