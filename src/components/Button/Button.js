import {Text, TouchableOpacity} from 'react-native';
import React from 'react';
import styles from './styles';

export default ({title, onPress, type = 'primary', style}) => {
  let styleContainer;
  let styleText;
  if (type === 'primary') {
    styleContainer = styles.primary_container;
    styleText = styles.primary_text;
  } else if (type === 'secondary') {
    styleContainer = styles.secondary_container;
    styleText = styles.secondary_text;
  }
  return (
    <TouchableOpacity
      style={[styleContainer, styles.btn_container, {...style}]}
      onPress={onPress}>
      <Text style={[styleText, styles.btn_text]}>{title}</Text>
    </TouchableOpacity>
  );
};
