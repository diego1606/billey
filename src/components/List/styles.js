import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  containerSquareList: {
    width: 120,
    height: 120,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    borderColor: '#e3e4e4',
    borderWidth: 1,
    margin: 15,
  },
  textSquareList: {
    fontSize: 35,
    marginBottom: 10,
  },
  textMinSquareList: {
    fontSize: 25,
    marginBottom: 10,
  },
});
