import React from 'react';
import {FlatList, Text, TouchableOpacity} from 'react-native';
import styles from './styles';
import {mainStyles} from '../../assets';

export const SquareList = ({data, onPress}) => (
  <FlatList
    data={data}
    numColumns={2}
    keyExtractor={(item, index) => index.toString()}
    renderItem={({item}) => {
      let styleText = styles.textSquareList;
      if (item.text.toString().length > 5) {
        styleText = styles.textMinSquareList;
      }
      return (
        <TouchableOpacity
          style={styles.containerSquareList}
          onPress={() => onPress(item)}>
          <Text style={styleText}>{item.text}</Text>
          <Text style={mainStyles.text}>{item.title}</Text>
        </TouchableOpacity>
      );
    }}
  />
);
