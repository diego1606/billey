import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  containerGraphTab: {
    height: 50,
    width: '100%',
    justifyContent: 'center',
  },
  contentGraphTab: {
    width: 100,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#707070',
    borderWidth: 1,
    marginRight: 10,
    borderRadius: 5,
  },
});
