import {FlatList, TouchableOpacity, View} from 'react-native';
import React from 'react';
import styles from './styles';
import {Icon} from 'react-native-elements';

export const GraphTab = ({onPress}) => {
  const data = [
    {
      name: 'chart-bar',
    },
    {
      name: 'chart-pie',
    },
  ];
  return (
    <View>
      <FlatList
        contentContainerStyle={styles.containerGraphTab}
        horizontal
        data={data}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({item}) => (
          <TouchableOpacity
            style={styles.contentGraphTab}
            onPress={() => onPress(item)}>
            <Icon type={'font-awesome-5'} name={item.name} testID={item.name} />
          </TouchableOpacity>
        )}
      />
    </View>
  );
};
