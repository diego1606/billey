import React from 'react';
import {View} from 'react-native';
import {mainStyles} from '../../assets';
import {Icon} from 'react-native-elements';

export default ({onPress}) => (
  <View style={mainStyles.btnMenuRight}>
    <Icon
      type={'font-awesome-5'}
      size={35}
      name="plus"
      onPress={() => onPress()}
    />
  </View>
);
