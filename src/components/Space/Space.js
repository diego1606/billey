import {View} from 'react-native';
import React from 'react';

export default ({height, width}) => <View style={{height, width}} />;
