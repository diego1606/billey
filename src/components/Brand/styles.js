import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  designByContainer: {
    marginBottom: 30,
  },
  designByText: {
    fontSize: 10,
    textAlign: 'center',
  },
  designByImgContainer: {
    alignItems: 'center',
  },
  designByImg: {
    width: 80,
    height: 40,
  },
});
