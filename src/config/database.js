import SQLite from 'react-native-sqlite-storage';
import {globals} from '../assets';

const DB = SQLite.openDatabase(
  {name: globals.NAME_DB, createFromLocation: 1, location: 'Library'},
  () => {},
  () => {
    console.error('An error occurred in the initialization of the database');
  },
);

class Database {
  db;
  constructor(db) {
    this.db = db;
  }
  executeSql = (sql: string, params = []) =>
    new Promise((resolve, reject) => {
      this.db.transaction((trans) => {
        trans.executeSql(
          sql,
          params,
          (db, results) => {
            resolve(results);
          },
          (error) => {
            reject(error);
          },
        );
      });
    });
}
export default new Database(DB);
