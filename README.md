# Project Billey

## Getting started
These instructions will get you a copy of the project up and running on your local machine for development and testing 
purposes.

### Prerequisites
Before you start cloning the project, please make sure that your development environment is set up properly. If it is not the case, please follow [this docs](https://reactnative.dev/docs/environment-setup)
 
### Installation
Clone the project and upload its dependencies 
```bash
https://gitlab.com/diego1606/billey.git
cd billey
yarn install
cd ios
pod install
```
Runs the app in the development mode.<br />
```bash
yarn react-native run-ios # ios
yarn react-native run-android # android
```

## Testing

Launches the test runner.<br />
```bash
yarn test
```
